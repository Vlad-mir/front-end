import Vue from 'vue'

export default {
  getFilter: function (options) {
    if (typeof options === 'undefined') return '';

    let currentPage = (typeof options.currentPage !== 'undefined') ? options.currentPage : 1,
      perPage       = (typeof options.perPage     !== 'undefined') ? options.perPage     : 100,
      orderBy       = (typeof options.sortBy      !== 'undefined') ? options.sortBy      : 'created_at',
      orderType     = (typeof options.sortOrder   !== 'undefined') ? options.sortOrder   : 'desc',
      symbol        = (typeof options.symbol      !== 'undefined') ? options.symbol      : '=',
      filterArray   = [],
      filter        = '',
      relationFilterArray = [];

    if (typeof options.filter !== 'undefined' && options.filter !== false) {
      for (let key in options.filter) {
        if (options.filter.hasOwnProperty(key)) {
          if ((key.indexOf('.') + 1)) {
            let relation = key.split('.')[0],
                field    = key.split('.')[1],
                value    = options.filter[key];
            if (value !== 'all') relationFilterArray.push('["' + relation + '", "' + field + '", "' + symbol +'", "' + value + '"]')
          } else {
            let value = options.filter[key];
            filterArray.push('["' + key + '", "' + symbol + '", "' + value + '"]')
          }
        }
      }
    }

    if (filterArray.length || relationFilterArray.length) {
      filter = '&filter=%7B';
      if (filterArray.length) filter += '"model": [' + filterArray.join(', "or", ') + ']';
      if (relationFilterArray.length) filter += (filterArray.length ? ', ' : '') + '"relation": [' + relationFilterArray.join(', "or", ') + ']';
      filter += '%7D'
    }

    if (options.filter === false && typeof options.filterString !== undefined) {
      filter = options.filterString;
    }

    return '?order_by=' + orderBy
      + '&order_type='  + orderType
      + '&page='  + currentPage
      + '&count=' + perPage
      + filter;

  }
}
