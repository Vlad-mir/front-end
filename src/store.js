import Vue from 'vue';
import Vuex from 'vuex';
import {stores as ModuleStores} from './views';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    endpoint: 'http://be-srv1.igid24.ru/api',
    moduleSettings: {
      header_phone: {
        value: ''
      },
      help_link: {
        value: ''
      }
    },
    snackbar: {
      show: false,
      duration: 4000,
      message: ''
    },
    location: {
      city: {
        name_ru: 'Хабаровск'
      }
    }
  },
  mutations: {
    showSnackbar(state, data) {
      let timeOut = (data[1]) ? data[1] : 4000;
      state.snackbar.duration = timeOut;
      state.snackbar.message  = data[0];
      state.snackbar.show     = true;
      setTimeout(function () {
        state.snackbar.show = false;
        state.snackbar.message = '';
      }, timeOut);
    },
    hideSnackbar(state) {
      state.snackbar.show    = false;
      state.snackbar.message = '';
    },
  },
  actions: {
    init({ dispatch }) {
      dispatch('getMainModuleSettings');
      dispatch('category/getCategoriesTree');
      dispatch('getUserLocation');
      dispatch('user/getCurrentUser').then(() => {
        dispatch('cart/getCart');
      });
    },
    getMainModuleSettings({ state, dispatch, rootState }, options) {
      let settings = localStorage.getItem('module_settings');
      if (settings) {
        Vue.set(state, 'moduleSettings', JSON.parse(settings));
      }

      return Vue.http.get(rootState.endpoint + '/module/module/props').then(
        response => {
          settings = response.body.response;
          Vue.set(state, 'moduleSettings', settings);
          localStorage.setItem('module_settings', JSON.stringify(settings));
          return settings;
        },
        response => {this.commit('showSnackbar', [response.body.errors.messages, 6000]);}
      );
    },

    getUserLocation({ state, dispatch, rootState }, options) {
      let location = localStorage.getItem('location');
      if (location) {
        Vue.set(state, 'location', JSON.parse(location));
      }
      return Vue.http.get(rootState.endpoint + '/userip').then(
        response => {
          location = response.body.response;
          Vue.set(state, 'location', location);
          localStorage.setItem('location', JSON.stringify(location));
          return location;
        },
        response => {this.commit('showSnackbar', [response.body.errors.messages, 6000]);}
      );
    }
  },
  modules: ModuleStores
});
