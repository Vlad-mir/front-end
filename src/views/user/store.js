import Vue from 'vue'

export default {
  store: {
    namespaced: true,
    state: {
      endpoint: 'user',
      currentUser: false,
      currentUserCompanies: {}
    },
    actions: {
      /**
       * Возвращает текущего авторизованного пользователя системы
       * @param state
       * @param dispatch
       * @param rootState
       * @param options
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      getCurrentUser({ state, dispatch, rootState }, options) {
        let currentUser = localStorage.getItem('current_user');
        if (currentUser) {
          Vue.set(state, 'currentUser', JSON.parse(currentUser));
        }
        return Vue.http.get(rootState.endpoint + '/user/current').then(
          response => {
            currentUser = response.body.response;
            Vue.set(state, 'currentUser', currentUser);
            localStorage.setItem('current_user', JSON.stringify(currentUser));
            return currentUser;
          }, response => {
            Vue.set(state, 'currentUser', false);
            this.commit('showSnackbar', ['Авторизуйтесь для того чтобы совершать покупки', 6000]);
          }
        );
      },

      /**
       * Получает компании, в которых участвует
       * пользователь с переданным id
       * @param state
       * @param dispatch
       * @param rootState
       * @param id
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      getCurrentUserCompanies({ state, dispatch, rootState }, id) {
        return Vue.http.get(rootState.endpoint + '/user/' + id + '/companies').then(
          response => {
            Vue.set(state, 'currentUserCompanies', response.body.response);
            return response.body.response;
          }, response => {
            Vue.set(state, 'currentUserCompanies', false);
            this.commit('showSnackbar', [response.body.errors.messages, 6000]);
          }
        );
      },

      /**
       * Авторизует пользователя,
       * устанавливает пользователя в state и в localStorage
       * @param state
       * @param dispatch
       * @param rootState
       * @param options
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      login({ state, dispatch, rootState }, options) {
        options.login = options.email;
        return Vue.http.post(rootState.endpoint + '/login', options).then(
          response => {
            let currentUser;
            currentUser = response.body.response;
            Vue.set(state, 'currentUser', currentUser);
            localStorage.setItem('id_token', currentUser.token_data.token);
            localStorage.setItem('current_user', JSON.stringify(currentUser));
            this.dispatch('init');
            return currentUser;
          }, response => {
            this.commit('showSnackbar', [response.body.errors.messages, 6000]);
            Vue.set(state, 'currentUser', false);
            return false;
          }
        );
      },


      /**
       * Авторизует пользователя,
       * устанавливает пользователя в state и в localStorage
       * @param state
       * @param dispatch
       * @param rootState
       * @param options
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      signup({ state, dispatch, rootState }, options) {
        options.login = options.email;
        delete options.login;
        return Vue.http.post(rootState.endpoint + '/signup', options).then(
          response => {
            let currentUser;
            currentUser = response.body.response;
            Vue.set(state, 'currentUser', currentUser);
            localStorage.setItem('id_token', currentUser.token_data.token);
            localStorage.setItem('current_user', JSON.stringify(currentUser));
            this.dispatch('init');
            return currentUser;
          }, response => {
            this.commit('showSnackbar', [response.body.errors.messages, 6000]);
            Vue.set(state, 'currentUser', false);
            return false;
          }
        );
      },



      /**
       * Сбрасывает пароль пользователя и отправляет ему
       * новый пароль на почту
       * @param state
       * @param dispatch
       * @param rootState
       * @param email
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      remind({ state, dispatch, rootState }, email) {
        return Vue.http.get(rootState.endpoint + '/user/' + email + '/break').then(
          response => {
            return response.body.response;
          }, response => {
            this.commit('showSnackbar', [response.body.errors.messages, 6000]);
            return false;
          }
        );
      },




    }
  }
}
