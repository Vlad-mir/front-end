import formLogin from './../../../../components/form-login/index.vue';

export default {
  name: 'login',
  components: {
    formLogin
  },
  data() {
    return {}
  },
  created() {},
  watch: {},
  computed: {
    /**
     * Вернёт текущего пользователя из стора
     * @returns {computed.currentUser|boolean}
     */
    currentUser() {
      return this.$store.state.user.currentUser;
    },
  },
  methods: {
  }
}
