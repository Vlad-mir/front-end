import breadcrumbs        from './../../../../components/breadcrumbs/index.vue';
import userPageNavigation from './../../../../components/user-page-navigation/index.vue';
import userPageSidebar    from './../../../../components/user-page-sidebar/index.vue';
import userPageProducts   from './../../../../components/user-page-products/index.vue';

export default {
  name: 'user-products',
  components: {
    breadcrumbs,
    userPageNavigation,
    userPageSidebar,
    userPageProducts
  },
  data() {
    return {}
  },
  created() {},
  watch: {},
  computed: {},
  methods: {
  }
}
