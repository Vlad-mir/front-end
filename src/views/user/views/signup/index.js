import formSignup from './../../../../components/form-signup/index.vue';

export default {
  name: 'signup',
  components: {
    formSignup
  },
  data() {
    return {}
  },
  created() {},
  watch: {},
  computed: {
    /**
     * Вернёт текущего пользователя из стора
     * @returns {computed.currentUser|boolean}
     */
    currentUser() {
      return this.$store.state.user.currentUser;
    },
  },
  methods: {
  }
}
