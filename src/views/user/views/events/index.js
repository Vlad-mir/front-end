import breadcrumbs        from './../../../../components/breadcrumbs/index.vue';
import userPageNavigation from './../../../../components/user-page-navigation/index.vue';
import userPageSidebar    from './../../../../components/user-page-sidebar/index.vue';
import userPageEvents     from './../../../../components/user-page-events/index.vue';

export default {
  name: 'events',
  components: {
    breadcrumbs,
    userPageNavigation,
    userPageSidebar,
    userPageEvents
  },
  data() {
    return {}
  },
  created() {},
  watch: {},
  computed: {},
  methods: {
  }
}
