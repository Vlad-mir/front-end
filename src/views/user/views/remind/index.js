import formRemind from './../../../../components/form-remind/index.vue';

export default {
  name: 'remind',
  components: {
    formRemind
  },
  data() {
    return {}
  },
  created() {},
  watch: {},
  computed: {
    /**
     * Вернёт текущего пользователя из стора
     * @returns {computed.currentUser|boolean}
     */
    currentUser() {
      return this.$store.state.user.currentUser;
    },
  },
  methods: {
  }
}
