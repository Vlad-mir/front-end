import User from './views/user/index.vue';
import UserProducts from './views/products/index.vue';
import UserEvents from './views/events/index.vue';
import Login from './views/login/index.vue';
import Signup from './views/signup/index.vue';
import Remind from './views/remind/index.vue';

export default {
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: {
        alias: 'Авторизация'
      }
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup,
      meta: {
        alias: 'Регистрация'
      }
    },
    {
      path: '/remind',
      name: 'Remind',
      component: Remind,
      meta: {
        alias: 'Восстановление пароля'
      }
    },
    {
      path: '/user',
      name: 'User',
      redirect: '/user/events',
      component: User,
      meta: {
        alias: 'Профиль'
      },
      children: [
        {
          path: '/user/products',
          name: 'UserProducts',
          component: UserProducts,
          meta: {
            alias: 'Мои товары'
          }
        },
        {
          path: '/user/events',
          name: 'UserEvents',
          component: UserEvents,
          meta: {
            alias: 'События'
          }
        }
      ]
    }

  ]
}
