import Category    from './views/category/index.vue';
import ProductView from './views/product/index.vue';
import ProductEdit from './views/product/edit/index.vue';

export default {
  routes: [
    {
      path: '/catalog',
      name: 'mainProductList',
      redirect: '/catalog/all',
      component: {
        render (c) { return c('router-view') }
      },
      meta: {
        alias: 'Каталог'
      },
      children: [
        {
          path: '/catalog/:category_id',
          name: 'categoryProductList',
          component: Category,
          meta: {
            alias: ''
          },
        },
        {
          path: '/catalog/:category_id/:product_id',
          name: 'ProductView',
          component: ProductView,
          meta: {
            alias: ''
          },
        },
        {
          path: '/catalog/:category_id/:product_id/edit',
          name: 'ProductEdit',
          component: ProductEdit,
          meta: {
            alias: ''
          },
        },
        {
          path: '/product/:product_id',
          name: 'ProductAdd',
          component: ProductEdit,
          meta: {
            alias: ''
          },
        }
      ]

    },
  ]

}
