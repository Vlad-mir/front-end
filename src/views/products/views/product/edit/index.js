import breadcrumbs from './../../../../../components/breadcrumbs/index.vue';
import formProductSku from './../../../../../components/form-product-sku/index.vue';
import formUpdateProduct from './../../../../../components/form-update-product/index.vue';
import formProductRemnants from './../../../../../components/form-product-remnants/index.vue';

export default {
  name: 'product-edit',
  components: {
    breadcrumbs,
    formProductSku,
    formUpdateProduct,
    formProductRemnants
  },
  data () {
    return {
      product: {}
    }
  },
  computed: {
    productId() {
      return this.$route.params.product_id;
    }
  },
  watch: {},
  methods: {},
  created:  function() {
  },
}
