import paginate    from 'vuejs-paginate';
import productItem from './../../../../components/product-item/index.vue';
import breadcrumbs from './../../../../components/breadcrumbs/index.vue';
import productListFilter   from './../../../../components/product-list-filter/index.vue';
import productListSettings from './../../../../components/product-list-settings/index.vue';

export default {
  name: 'product',
  components: {
    paginate,
    breadcrumbs,
    productItem,
    productListFilter,
    productListSettings
  },
  data () {
    return {
      per_page: 10,    //элементов на странице
      current_page: 0  //текущая страница (чтобы установить первую страницу компоненту надо передать 0 ¯\_(ツ)_/¯)
    }
  },
  computed: {
    /**
     * Все товары
     * @returns {Array}
     */
    products() {
      return this.$store.state.products.products.data;
    },

    /**
     * Всего событий
     * @returns {number}
     */
    total() {
      return this.$store.state.products.products.total;
    },

    /**
     * Последняя страница
     * @returns {number}
     */
    lastPage() {
      return this.$store.state.products.products.last_page;
    },

    /**
     * Выбранный тип отображения товаров
     * @returns {computed.sortType|string|*}
     */
    sortType() {
      return this.$store.state.products.sortType;
    },

    /**
     * Выбранное направление сортировки
     * @returns {computed.viewType|string|*}
     */
    viewType() {
      return this.$store.state.products.viewType;
    },

    /**
     * Плоский список всех категорий товаров
     * @returns {computed.categories|Array}
     */
    categories() {
      return this.$store.state.category.categories;
    },


    /**
     * ID выбранной пользователем категории,
     * @returns {null}
     */
    currentCategoryId() {
      let category = this.$route.params.category_id;
      if (!category || category === 'all') {
        category = null;
      }
      return category;
    },

    /**
     * Выбранная пользователем категория
     * @returns {{}}
     */
    currentCategory() {
      let cat = {};
      if (this.categories && this.categories.length) {
        this.categories.map(item => {
          if (parseInt(item.id) === parseInt(this.currentCategoryId)) {cat = item;}
        });
      }
      return cat;
    },
  },
  watch: {
    /**
     * Сбрасываем страницу у пагинатора
     * когда пользователь меняет категорию
     */
    '$route.params.category_id'() {
      this.current_page = 0;
    },
    /**
     * Меняем название алиаса у роута для хлебных крошек
     * когда пользователь изменяет категорию
     * @param category
     */
    'currentCategory'(category) {
      this.changeBreadCrumbsAlias(category);
    }
  },
  methods: {
    changeBreadCrumbsAlias(category){
      if (Object.keys(category).length) {
        this.$route.meta.alias = this.currentCategory.name;
      } else {
        this.$route.meta.alias = 'Все категории';
      }
    },

    /**
     * Получение страничного вывода списка товаров
     * @param count   - количество товаров на странице
     * @param page    - страница, которую надо показать
     * @param filter  - фильтр
     */
    getProducts(count, page, filter) {
      count   = (count)   ? count   : this.per_page;
      page    = (page)    ? page    : 1;
      filter  = (filter)  ? filter  : null;
      this.$store.dispatch(
        'products/getProducts',
        {count: count, page: page, filter: filter}
      ).then(() => {
        this.current_page = 0;
      });
    },
    /**
     * Событие, вызывающееся когда юзер кликает по пагинатору
     * @param page - номер страницы, на который нажал юзер
     */
    paginatorAction(page) {
      this.getProducts(this.per_page, page, this.$refs.filter.getFilter());
    },
    /**
     * Запускает фильтрацию товаров
     * Вызывает событием из компонента product-list-filter
     */
    filterProducts() {
      this.getProducts(
        this.per_page,
        this.current_page,
        this.$refs.filter.getFilter()
      );
    }
  },
  created:  function() {
    this.$nextTick(() => {
      this.getProducts(false, false, this.$refs.filter.getFilter());
      this.changeBreadCrumbsAlias(this.currentCategory);
      this.$refs.breadcrumbs.getBreadCrumbs();
    });

  },

}
