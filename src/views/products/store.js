import Vue from 'vue';
import {helpers} from './../../helpers';

export default {
  store: {
    namespaced: true,
    state: {
      endpoint: 'products',
      /**
       * Товар, который открыл пользователь
       */
      product: {},
      /**
       * Товары, принадлежащие текущему пользователю
       */
      userProduct: {
        total: 1,
        last_page: 1,
        data: []
      },
      /**
       * Список товаров на странице товаров
       */
      products: {
        total: 1,
        last_page: 1,
        data: []
      },
      /**
       * Все типы представления списка товаров
       */
      viewTypes: {
        grid: 'fa-th',
        list: 'fa-bars'
      },
      /**
       * Все типы сортировки
       */
      sortTypes: {
        updated_at: {label: 'По дате добавления', dir: 'asc',  field: 'updated_at'},
        rating:     {label: 'По рейтингу',        dir: 'asc',  field: 'rating'},
        price_min:  {label: 'По цене: с меньшей', dir: 'asc',  field: 'price'},
        price_max:  {label: 'По цене: с большей', dir: 'desc', field: 'price'}
      },
      /**
       * Выбранный тип отображения товаров
       */
      sortType: 'updated_at',
      /**
       * Выбранное направление сортировки
       */
      viewType: 'grid',
    },
    mutations: {
      /**
       * Изменяет направление сортировки товаров
       * @param state
       * @param type
       * @returns {*}
       */
      changeSortType(state, type) {
        localStorage.setItem('product_sort_type', type);
        state.sortType = type;
        return type;
      },
      /**
       * Изменяет тип отображения списка товаров
       * @param state
       * @param type
       * @returns {*}
       */
      changeViewType(state, type) {
        localStorage.setItem('product_view_type', type);
        state.viewType = type;
        return type;
      },
      /**
       * Проверяет дефолтные значения представления и сортировки
       * в localStorage и устанавливает их, если не заданы
       * @param state
       */
      checkSetting(state) {
        let viewType = localStorage.getItem('product_view_type'),
            sortType = localStorage.getItem('product_sort_type');
        if (viewType) state.viewType = viewType;
        if (sortType) state.sortType = sortType;
      },
    },
    actions: {
      /**
       * Получение списка товаров у пользователя
       * @param state
       * @param dispatch
       * @param rootState
       * @param options
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      getUserProduct({ state, dispatch, rootState }, options) {
        if (!options.id) return;
        let params = {
          page:       (options && options.page)  ? options.page  : 1,
          count:      (options && options.count) ? options.count : 10,
          order_by:   state.sortTypes[state.sortType]['field'],
          order_type: state.sortTypes[state.sortType]['dir'],
          filter:     {model: [], relation: []}
        };
        //фильтр всегда должен возвращать только дефолтные SKU
        params.filter.model.push(['default','=',1]);

        return Vue.http.get(rootState.endpoint + '/user/' + options.id + '/products', {params: params}).then(
          response => {
            Vue.set(state, 'userProduct', response.body.response);
            return response;
          }, response => {
            Vue.set(state, 'userProduct', []);
            this.commit('showSnackbar', [response.body.errors.messages, 60000]);
          }
        );
      },

      /**
       * Получение списка товаров
       * @param state
       * @param dispatch
       * @param rootState
       * @param options
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      getProducts({ state, dispatch, rootState }, options) {
        let params = {
          page:       (options && options.page)  ? options.page  : 1,
          count:      (options && options.count) ? options.count : 10,
          order_by:   state.sortTypes[state.sortType]['field'],
          order_type: state.sortTypes[state.sortType]['dir'],
          filter:     {model: [], relation: []}
        };

        //добавляем фильтр
        if (options.filter) {
          params.filter = options.filter;
          options.filter.model = [];
        }
        //фильтр всегда должен возвращать только дефолтные SKU
        params.filter.model.push(['default','=',1]);
        //добавляем условие для поиска SKU только у опубликованных товаров
        params.filter.relation.push(['product','public','=',2]);

        return Vue.http.get(rootState.endpoint + '/sku', {params: params}).then(
          response => {
            Vue.set(state, 'products', response.body.response);
            return response;
          }, response => {
            Vue.set(state, 'products', []);
            this.commit('showSnackbar', [response.body.errors.messages, 60000]);
          }
        );
      },

      /**
       * Возвращает товар по id
       * @param state
       * @param dispatch
       * @param rootState
       * @param id
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      getProductByid({ state, dispatch, rootState }, id) {
        return Vue.http.get(rootState.endpoint + '/product/' + id).then(
          response => {
            Vue.set(state, 'product', response.body.response);
            return response.body.response;
          }, response => {
            Vue.set(state, 'product', {});
            this.commit('showSnackbar', [response.body.errors.messages, 60000]);
            return response;
          }
        );
      },

      /**
       * Возвращает товар по id торгового предложения
       * @param state
       * @param dispatch
       * @param rootState
       * @param id
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      getProductBySKUid({ state, dispatch, rootState }, id) {
        return Vue.http.get(rootState.endpoint + '/sku/' + id + '/product').then(
          response => {
            Vue.set(state, 'product', response.body.response);
            return response.body.response;
          }, response => {
            Vue.set(state, 'product', {});
            this.commit('showSnackbar', [response.body.errors.messages, 60000]);
            return response;
          }
        );
      },

      /**
       * Обновление товара
       * @param state
       * @param dispatch
       * @param rootState
       * @param data
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      putProduct({ state, dispatch, rootState }, data) {
        data['_method'] = 'PUT';
        return Vue.http.post(rootState.endpoint + '/product', data).then(
          response => {
            this.commit('showSnackbar', ['Товар успешно обновлён', 6000]);
            return response.body.response;
          }, response => {
            this.commit('showSnackbar', [response.body.errors.messages, 60000]);
            return response;
          }
        );
      },

      /**
       * Добавление товара
       * @param state
       * @param dispatch
       * @param rootState
       * @param data
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      postProduct({ state, dispatch, rootState }, data) {
        return Vue.http.post(rootState.endpoint + '/product', data).then(
          response => {
            this.commit('showSnackbar', ['Товар успешно добавлен', 6000]);
            return response.body.response;
          }, response => {
            this.commit('showSnackbar', [response.body.errors.messages, 60000]);
            return response;
          }
        );
      },

      /**
       * Удаление товара
       * @param state
       * @param dispatch
       * @param rootState
       * @param id
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      removeProduct({ state, dispatch, rootState }, id) {
        return Vue.http.delete(rootState.endpoint + '/product/' + id).then(
          response => {
            this.commit('showSnackbar', ['Товар успешно удалён', 6000]);
            return response.body.response;
          }, response => {
            this.commit('showSnackbar', [response.body.errors.messages, 60000]);
            return response;
          }
        );
      },

    }
  }
}
