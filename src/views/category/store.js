import Vue from 'vue'

export default {
  store: {
    namespaced: true,
    state: {
      endpoint: 'category',
      cart: false,
      categoriesTree: [],
      categories: [],
      categoryProperties: [],
      propertiesValues: {}
    },
    actions: {

      /**
       * Возвращает иерархическое дерево категорий каталога
       * @param state
       * @param dispatch
       * @param rootState
       * @param options
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      getCategoriesTree({ state, dispatch, rootState }, options) {
        let categoriesTree = localStorage.getItem('categories_tree');
        if (categoriesTree) {
          Vue.set(state, 'categoriesTree', JSON.parse(categoriesTree));
        }
        return Vue.http.get(rootState.endpoint + '/category/tree/get').then(
          response => {
            categoriesTree = response.body.response;
            localStorage.setItem('categories_tree', JSON.stringify(categoriesTree));
            Vue.set(state, 'categoriesTree', categoriesTree);
            return response;
          }, response => {
            Vue.set(state, 'categoriesTree', []);
            this.commit('showSnackbar', [response.body.errors.messages, 6000]);
          }
        );
      },

      /**
       * Возвращает список категорий каталога
       * @param state
       * @param dispatch
       * @param rootState
       * @param options
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      getCategories({ state, dispatch, rootState }, options) {
        let categories = localStorage.getItem('categories');
        if (categories) {
          Vue.set(state, 'categories', JSON.parse(categories));
        }
        return Vue.http.get(rootState.endpoint + '/category').then(
          response => {
            categories = response.body.response;
            localStorage.setItem('categories', JSON.stringify(categories));
            Vue.set(state, 'categories', categories);
            return response;
          }, response => {
            Vue.set(state, 'categories', []);
            this.commit('showSnackbar', [response.body.errors.messages, 6000]);
          }
        );
      },

      /**
       * Возвращает список свойств, доступных для категории
       * @param state
       * @param dispatch
       * @param rootState
       * @param categories
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      getCategoryProperties({ state, dispatch, rootState }, categories) {
        return Vue.http.get(rootState.endpoint + '/properties', {params: {categories:  categories}}).then(
          response => {
            Vue.set(state, 'categoryProperties', response.body.response);
            return response;
          }, response => {
            Vue.set(state, 'categoryProperties', []);
            this.commit('showSnackbar', [response.body.errors.messages, 6000]);
          }
        );
      },


      /**
       * Возвращает список значений свойств для СКУ
       * @param state
       * @param dispatch
       * @param rootState
       * @param sku_id
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      getSKUPropertiesValues({ state, dispatch, rootState }, sku_id) {
        return Vue.http.get(rootState.endpoint + '/sku/' + sku_id +'/values').then(
          response => {
            let newValue = state.propertiesValues;
            newValue[sku_id] = response.body.response;
            Vue.set(state, 'propertiesValues', newValue);
            return response;
          }, response => {
            this.commit('showSnackbar', [response.body.errors.messages, 6000]);
          }
        );
      },


    }
  }
}
