import Vue from 'vue'

export default {
  store: {
    namespaced: true,
    state: {
      endpoint: 'log',
      userEvents: {
        total: 1,
        last_page: 1,
        data: []
      },
    },
    actions: {
      getUserEvents({ state, dispatch, rootState }, options) {
        if (options === undefined) {
          options = {
            count: 10,
            page: 1
          };
        }
        let filter = '?count=' + ((options.count) ? options.count : 1) + '&page=' + ((options.page) ? options.page : 1);

        return Vue.http.get(rootState.endpoint + '/events' + filter).then(
          response => {
            Vue.set(state, 'userEvents', response.body.response);
            return response;
          }, response => {
            Vue.set(state, 'userEvents', []);
            this.commit('showSnackbar', [response.body.errors.messages, 60000]);
          }
        );
      },




    }
  }
}
