import Vue from 'vue'

export default {
  store: {
    namespaced: true,
    state: {
      endpoint: 'home'
    },
    actions: {}
  }
}
