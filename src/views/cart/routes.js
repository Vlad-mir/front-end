import Cart from './views/cart/index.vue';

export default {
  routes: [
    {
      path: '/cart',
      name: 'Cart',
      component: Cart,
      meta: {
        alias: 'Корзина'
      }
    }
  ]
}
