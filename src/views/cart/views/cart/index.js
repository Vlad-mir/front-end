import breadcrumbs from './../../../../components/breadcrumbs/index.vue';
import formCartProducts from './../../../../components/form-cart-products/index.vue'

export default {
  name: 'cart',
  components: {
    formCartProducts,
    breadcrumbs
  },
  data() {
    return {}
  },
  created() {},
  watch: {},
  computed: {},
  methods: {
  }
}
