import Vue from 'vue'

export default {
  store: {
    namespaced: true,
    state: {
      endpoint: 'cart',
      cart: {},
    },
    actions: {
      /**
       * Возвращает корзину пользователя с товарами
       * @param state
       * @param dispatch
       * @param rootState
       * @param options
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      getCart({ state, dispatch, rootState }, options) {
        let cartCode = localStorage.getItem('cart_code');
        if (cartCode) {
          cartCode = '?cart_code=' + cartCode;
        } else {
          cartCode = '';
        }
        return Vue.http.get(rootState.endpoint + '/user_cart' + cartCode).then(
          response => {
            Vue.set(state, 'cart', response.body.response);
            localStorage.setItem('cart_code', response.body.response.code);
            return response;
          }, response => {
            Vue.set(state, 'cart_code', false);
            this.commit('showSnackbar', [response.body.errors.messages, 6000]);
            return false;
          }
        );
      },

      /**
       * Добавляетс СУК в корзину
       * @param state
       * @param dispatch
       * @param rootState
       * @param data
       * @returns {PromiseLike<T>}
       */
      pushSKUToCart({ state, dispatch, rootState }, data) {
        return Vue.http.post(rootState.endpoint + '/product_in_cart', data).then(
          response => {
            Vue.set(state, 'cart', response.body.response);
            return response.body.response;
          }, response => {
            this.commit('showSnackbar', [response.body.errors.messages, 6000]);
          }
        );
      },
      /**
       * Добавляетс СУК в корзину
       * @param state
       * @param dispatch
       * @param rootState
       * @param data
       * @returns {PromiseLike<T>}
       */
      removeSKUFromCart({ state, dispatch, rootState }, data) {
        return Vue.http.delete(rootState.endpoint + '/cart/' + data.cartCode + '/sku/' + data.skuId).then(
          response => {
            Vue.set(state, 'cart', response.body.response);
            return response.body.response;
          }, response => {
            this.commit('showSnackbar', [response.body.errors.messages, 6000]);
          }
        );
      },


    }
  }
}
