import Vue from 'vue';
import {helpers} from './../../helpers';

export default {
  store: {
    namespaced: true,
    state: {
      endpoint: 'sku',
      sku: {}
    },
    mutations: {},
    actions: {
      /**
       * Добавляет торговое предложение
       * @param state
       * @param dispatch
       * @param rootState
       * @param options
       * @returns {PromiseLike<T>}
       */
      postSKU({ state, dispatch, rootState }, options) {
        return Vue.http.post(rootState.endpoint + '/sku', options).then(
          response => {
            Vue.set(state, 'sku', response.body.response);
            this.commit('showSnackbar', ['СКУ успешно добавлено', 6000]);
            return response.body.response;
          }, response => {
            Vue.set(state, 'sku', {});
            this.commit('showSnackbar', [response.body.errors.messages, 60000]);
            return response;
          }
        );
      },

      /**
       * Удаляет изобрадение у торгового предложения
       * @param state
       * @param dispatch
       * @param rootState
       * @param id
       * @returns {PromiseLike<T>}
       */
      deleteSKUHeroImage({ state, dispatch, rootState }, data) {
        return Vue.http.delete(
          rootState.endpoint + '/sku/' + data.sku_id +  '/image/' + data.img_id
        ).then(
          response => {
            this.commit('showSnackbar', ['Изображение СКУ успешно удалено', 6000]);
            return response.body.response;
          }, response => {
            this.commit('showSnackbar', [response.body.errors.messages, 60000]);
            return response;
          }
        );
      },

      /**
       * Удаляет торговое предложение
       * @param state
       * @param dispatch
       * @param rootState
       * @param id
       * @returns {PromiseLike<T>}
       */
      deleteSKU({ state, dispatch, rootState }, id) {
        return Vue.http.delete(rootState.endpoint + '/sku/' + id).then(
          response => {
            this.commit('showSnackbar', ['СКУ успешно удалено', 6000]);
            return response.body.response;
          }, response => {
            Vue.set(state, 'sku', {});
            this.commit('showSnackbar', [response.body.errors.messages, 60000]);
            return response;
          }
        );
      },

      /**
       * Обновляет торговое предложение
       * @param state
       * @param dispatch
       * @param rootState
       * @param options
       * @returns {PromiseLike<T>}
       */
      putSKU({ state, dispatch, rootState }, options) {
        options.append('_method', 'PUT');
        return Vue.http.post(rootState.endpoint + '/sku', options).then(
          response => {
            Vue.set(state, 'sku', response.body.response);
            this.commit('showSnackbar', ['СКУ успешно обновленно', 6000]);
            return response.body.response;
          }, response => {
            Vue.set(state, 'sku', {});
            this.commit('showSnackbar', [response.body.errors.messages, 60000]);
            return response;
          }
        );
      },

    }
  }
}
