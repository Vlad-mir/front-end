import Vue from 'vue'

export default {
  store: {
    namespaced: true,
    state: {
      endpoint: 'properties',
    },
    actions: {
      /**
       * Добавление/изменение значений свойств какой-либо сущности
       * @param state
       * @param dispatch
       * @param rootState
       * @param data
       * @returns {PromiseLike<T>}
       */
      postPropertiesValue({ state, dispatch, rootState }, data) {
        return Vue.http.post(rootState.endpoint + '/properties/values', data).then(
          response => {
            console.log(response.body);
            return response;
          }, response => {
            this.commit('showSnackbar', [response.body.errors.messages, 6000]);
          }
        );
      },

      /**
       * Удаляет значение свойства файла и сам файл
       * @param state
       * @param dispatch
       * @param rootState
       * @param data
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      deleteImage({ state, dispatch, rootState }, data) {
        return Vue.http.delete(
          rootState.endpoint + '/property/value', {
            params: {
              property_id: data.property_id,
              entity_id:   data.entity_id,
              value:       data.value
            }
          }
        ).then(
          response => {
            this.commit('showSnackbar', ['Изображение СКУ успешно удалено', 6000]);
            return response.body.response;
          }, response => {
            this.commit('showSnackbar', [response.body.errors.messages, 60000]);
            return response;
          }
        );
      },


    }
  }
}
