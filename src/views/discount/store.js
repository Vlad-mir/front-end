import Vue from 'vue';
import {helpers} from './../../helpers';

export default {
  store: {
    namespaced: true,
    state: {
      endpoint: 'discount',
      discounts: {
        total: 1,
        last_page: 1,
        data: []
      },
      /**
       * Все типы сортировки
       */
      sortTypes: {
        updated_at: {label: 'По дате добавления', dir: 'asc',  field: 'updated_at'},
        rating:     {label: 'По рейтингу',        dir: 'asc',  field: 'rating'},
        price_min:  {label: 'По цене: с меньшей', dir: 'asc',  field: 'price'},
        price_max:  {label: 'По цене: с большей', dir: 'desc', field: 'price'}
      },
      /**
       * Выбранный тип отображения товаров
       */
      sortType: 'updated_at',
    },
    mutations: {},
    actions: {
      /**
       * Получение списка товаров
       * @param state
       * @param dispatch
       * @param rootState
       * @param options
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      getDiscounts({ state, dispatch, rootState }, data) {
        let params = {
          page:       (data && data.page)  ? data.page  : 1,
          count:      (data && data.count) ? data.count : 10,
          order_by:   state.sortTypes[state.sortType]['field'],
          order_type: state.sortTypes[state.sortType]['dir'],
          filter:     (data.filter) ? data.filter : {model: [], relation: []}
        };
        return Vue.http.get(rootState.endpoint + '/discount', {params: params}).then(
          response => {
            Vue.set(state, 'discounts', response.body.response);
            return response;
          }, response => {
            Vue.set(state, 'discounts', []);
            this.commit('showSnackbar', [response.body.errors.messages, 60000]);
          }
        );


      },


    }
  }
}
