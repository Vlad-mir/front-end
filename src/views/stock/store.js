import Vue from 'vue';
import {helpers} from './../../helpers';

export default {
  store: {
    namespaced: true,
    state: {
      endpoint: 'stock',
      remnants: {},
      stocks: []
    },
    mutations: {
      /**
       * Изменяет направление сортировки товаров
       * @param state
       * @param type
       * @returns {*}
       */
      changeSortType(state, type) {
        localStorage.setItem('product_sort_type', type);
        state.sortType = type;
        return type;
      },
      /**
       * Изменяет тип отображения списка товаров
       * @param state
       * @param type
       * @returns {*}
       */
      changeViewType(state, type) {
        localStorage.setItem('product_view_type', type);
        state.viewType = type;
        return type;
      },
      /**
       * Проверяет дефолтные значения представления и сортировки
       * в localStorage и устанавливает их, если не заданы
       * @param state
       */
      checkSetting(state) {
        let viewType = localStorage.getItem('product_view_type'),
          sortType = localStorage.getItem('product_sort_type');
        if (viewType) state.viewType = viewType;
        if (sortType) state.sortType = sortType;
      },
    },
    actions: {
      /**
       * Сохранение нового остатка
       * @param state
       * @param dispatch
       * @param rootState
       * @param data
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      postRemnant({ state, dispatch, rootState }, data) {
        return Vue.http.post(rootState.endpoint + '/remnant', data).then(
          response => {
            this.commit('showSnackbar', ['Остаток успешно добавлен', 6000]);
            return response.body.response;
          }, response => {
            this.commit('showSnackbar', [response.body.errors.messages, 60000]);
            return response;
          }
        );
      },

      /**
       * Обновление нового остатка
       * @param state
       * @param dispatch
       * @param rootState
       * @param data
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      putRemnant({ state, dispatch, rootState }, data) {
        data['_method'] = 'PUT';
        return Vue.http.post(rootState.endpoint + '/remnant', data).then(
          response => {
            this.commit('showSnackbar', ['Остаток успешно обновлён', 6000]);
            return response.body.response;
          }, response => {
            this.commit('showSnackbar', [response.body.errors.messages, 60000]);
            return response;
          }
        );
      },

      /**
       * Удаление остатка
       * @param state
       * @param dispatch
       * @param rootState
       * @param id
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      removeRemnant({ state, dispatch, rootState }, id) {
        return Vue.http.delete(rootState.endpoint + '/remnant/' + id).then(
          response => {
            this.commit('showSnackbar', ['Остаток успешно удалён', 6000]);
            return response.body.response;
          }, response => {
            this.commit('showSnackbar', [response.body.errors.messages, 60000]);
            return response;
          }
        );
      },

      /**
       * Получение остатков всех СКУ у товара
       * @param state
       * @param dispatch
       * @param rootState
       * @param id
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      getProductRemnants({ state, dispatch, rootState }, id) {
        return Vue.http.get(rootState.endpoint + '/product/' + id + '/remnant').then(
          response => {
            Vue.set(state, 'remnants', response.body.response);
            return response.body.response;
          }, response => {
            this.commit('showSnackbar', [response.body.errors.messages, 60000]);
            Vue.set(state, 'remnants', []);
            return response;
          }
        );
      },

      /**
       * Возвращает все склады доступные пользователю
       * и указанной компании
       * @param state
       * @param dispatch
       * @param rootState
       * @param data
       * @returns {*|PromiseLike<T>|Promise<T>}
       */
      getUserStock({ state, dispatch, rootState }, data) {
        return Vue.http.get(
          rootState.endpoint + '/user/' + data.user_id + '/company/' + data.company_id + '/stock'
        ).then(
          response => {
            Vue.set(state, 'stocks', response.body.response);
            return response.body.response;
          }, response => {
            this.commit('showSnackbar', [response.body.errors.messages, 60000]);
            Vue.set(state, 'stocks', []);
            return response;
          }
        );
      },

    }
  }
}
