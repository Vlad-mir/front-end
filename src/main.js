import Vue                from 'vue';
import App                from './App.vue';
import VueRouter          from 'vue-router';
import VueResource        from 'vue-resource';
import VeeValidate        from 'vee-validate';
import ValidationMessages from 'vee-validate/dist/locale/ru';
import Moment             from 'moment';
import VueInputMask       from 'vue-inputmask';
import { sync }           from 'vuex-router-sync';
import { store }          from './store';
import { helpers }        from "./helpers";
import {
  routes as ModuleRoutes,
  stores as ModuleStores
} from './views';

import fontAwesome from './assets/css/font-awesome/css/font-awesome.css';
import templateCSS from './assets/css/template.css';
import correctionsCSS from './assets/css/corrections.css';
//import templateMobileCSS from './assets/css/mobile-template.css';


ValidationMessages.attributes = {
  email:            '"Email адрес"',
  password:         '"Пароль"',
  password_confirm: '"Подтверждение пароля"',
  regcheck: '"Договор и публичная оферта"'
};

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(VueInputMask);
Vue.use(VeeValidate, {
  locale: 'ru',
  events: 'blur',
  dictionary: {
    ru: ValidationMessages
  }
});




Vue.prototype.moment = Moment;
Vue.http.headers.common['Authorization']    = localStorage.getItem('id_token');
Vue.http.headers.common['Accept']           = 'application/json, text/plain, */*';
Vue.http.headers.common['X-Requested-With'] = 'XMLHttpRequest';
Vue.http.headers.common['Access-Control-Allow-Origin'] = '*';
//Vue.http.options.credentials = true;
Vue.http.options.emulateHTTP = true;


let router = new VueRouter({
  'mode': 'history',
  assetsPublicPath: '',
  scrollBehavior: () => ({ y: 0 }),
  linkActiveClass: 'router-link-active active',
  routes: [].concat(ModuleRoutes)
});
const unsync = sync(store, router);


import breadcrumbs from './components/breadcrumbs/index.vue';


let components = {
  App,
  breadcrumbs
};


new Vue({
  router,
  store,
  components: components,
  el: '#app',
  template: '<app/>',
  watch: {},
  helpers: helpers,
  created: function () {
    this.$store.dispatch('init');
    document.addEventListener('DOMContentLoaded', function() {
      console.log('Loaded');
    });
    this.$nextTick(function () {});
  },
  render: h => h(App)
});
