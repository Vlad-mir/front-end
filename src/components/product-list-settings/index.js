export default {
  name: 'product-list-settings',
  components: {},
  props: [],
  data() {
    return {
      showSort: false
    }
  },
  created() {
    this.checkSetting();
  },
  watch: {},
  computed: {
    /**
     * Выбранный тип отображения товаров
     * @returns {computed.sortType|string|*}
     */
    sortType() {
      return this.$store.state.products.sortType;
    },
    /**
     * Выбранное направление сортировки
     * @returns {computed.viewType|string|*}
     */
    viewType() {
      return this.$store.state.products.viewType;
    },
    /**
     * Возвращает все типы сортировки
     * @returns {computed.sortTypes|{updated_at: string, rating: string, price_min: string, price_max: string}|store.state.sortTypes|{updated_at, rating, price_min, price_max}}
     */
    sortTypes() {
      return this.$store.state.products.sortTypes
    },
    /**
     * Возвращает все типы представления списка товаров
     * @returns {computed.viewTypes|{grid: string, list: string}|store.state.viewTypes|{grid, list}}
     */
    viewTypes() {
      return this.$store.state.products.viewTypes;
    }
  },
  methods: {
    /**
     * Проверяет дефолтные значения представления и сортировки
     * в localStorage и устанавливает их, если не заданы
     */
    checkSetting() {
      this.$store.commit('products/checkSetting');
    },
    /**
     * Тугглит значение для вывода/скрытия селектора сортировки
     */
    toggleSortTypes() {
      this.showSort = !this.showSort;
    },
    /**
     * Изменяет направление сортировки товаров
     * @param type
     * @returns {*}
     */
    changeSortType(type) {
      this.$store.commit('products/changeSortType', type);
      this.showSort = false;
      this.$emit('change');
    },
    /**
     * Изменяет тип отображения списка товаров
     * @param type
     * @returns {*}
     */
    changeViewType(type) {
      this.$store.commit('products/changeViewType', type);
      this.$emit('change');
    }
  }
}
