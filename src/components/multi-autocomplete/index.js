import css from './index.css';


export default {
  name: 'multi-autocomplete',
  props: [
    'values',
    'title',
    'value',
    'required'
  ],
  components: {},
  data () {
    return {
      dropdown: false,
      resultValue: [],
      search: '',
      filtered: []
    }
  },
  computed: {},
  watch: {
    'values'() {
      if (this.value && this.value.length) {
        this.values.map(item => {
          if (!this.inValue(item.value) && this.value.indexOf(item.value) !== -1) {
            this.resultValue.push(item);
          }
        });
      }
    },
    'value'(newVal) {
      if (newVal && newVal.length) {
        this.values.map(item => {
          if (!this.inValue(item.value) && newVal.indexOf(item.value) !== -1) {
            this.resultValue.push(item);
          }
        });
      }
    }
  },
  methods: {
    searchValue() {
      this.filtered = [];
      this.values.map(item => {
        if((!this.inValue(item.value) && this.search && item.text.toLowerCase().indexOf(this.search.toLowerCase()) !== -1)
        ||
        (!this.inValue(item.value) && !this.search)
        ) {
          this.filtered.push(item);
        }
      });
    },
    add(val) {
      this.resultValue.push(val);
      this.changeValue();
    },
    remove(val) {
      let result = [];
      this.resultValue.forEach(item => {
        if (item.value !== val.value) {
          result.push(item);
        }
      });
      this.resultValue = Object.assign([], result);
      this.changeValue();
    },
    inValue(val) {
      let result = false;
      this.resultValue.forEach(item => {
        if (item.value === val) {
          result = true;
        }
      });
      return result;
    },
    hideDropdown() {
      setTimeout(() => {
        this.dropdown = false;
      }, 100);
    },
    changeValue() {
      let result = [];
      if (this.resultValue.length) {
        this.resultValue.map(item => result.push(item.value));
      }
      this.$emit('input', result);
    }
  },
  created: function() {
    this.searchValue();
  },

}
