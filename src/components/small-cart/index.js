export default {
  name: 'small-cart',
  props: [],
  components: {},
  data() {
    return {}
  },
  created() {},
  watch: {},
  computed: {
    productInCart() {
      return this.$store.state.cart.cart;
    },
  },
  methods: {}
}
