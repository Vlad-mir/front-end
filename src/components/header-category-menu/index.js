export default {
  name: 'header-category-menu',
  props: [],
  components: {},
  data() {
    return {}
  },
  created() {},
  watch: {},
  computed: {
    categories() {
      return this.$store.state.category.categoriesTree;
    },
  },
  methods: {}
}
