export default {
  name: 'input-select',
  props: [
    'id',
    'name',
    'error',
    'value',
    'label',
    'values',
    'required',
    'value-code',
    'value-label',
  ],
  components: {},
  data () {
    return {
      inputValue: ''
    }
  },
  computed: {

  },
  watch: {
    'inputValue' (newValue) {
      this.$emit('input', newValue);
      this.$emit('change', newValue);
    },
    '$props.value'(value) {
      this.inputValue = value;
    }
  },
  methods: {},
  created:  function() {
    this.$nextTick(() => {
      this.inputValue = this.$props.value;
    });
  },
}
