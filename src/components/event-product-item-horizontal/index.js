import productActionBar from './../product-action-bar/index.vue';

export default {
  name: 'event-product-item-horizontal',
  props: ['product'],
  components: {
    productActionBar
  },
  data() {
    return {}
  },
  created() {},
  watch: {},
  computed: {
  },
  methods: {
    removeProduct() {
      this.$emit('removeProduct');
    }
  }
}
