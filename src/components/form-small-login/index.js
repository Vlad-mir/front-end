export default {
  name: 'form-small-login',
  props: [],
  data() {
    return {
      //объект формы
      from: {
        login: '',
        password: ''
      },
      //правила валидации формы
      rules: {
        email: {
          required: true,
          email: true
        },
        password: {
          required: true
        },
      }
    }
  },
  created() {},
  watch: {},
  computed: {
    /**
     * Вернёт текущего пользователя из стора
     * @returns {computed.currentUser|boolean}
     */
    currentUser() {
      return this.$store.state.user.currentUser;
    },
  },
  methods: {
    /**
     * Валидирует форму и авторизует пользователя
     * @param e - событие submit'a формы
     */
    submit(e) {
      e.preventDefault();
      this.$validator.validateAll().then((result) => {
        if (result) {
          this.$store.dispatch('user/login', this.from).then(response => {
            if (response) document.location.reload();
          });
        }
      });
    }
  }
}
