import productActionBar from './../product-action-bar/index.vue';

export default {
  name: 'product-item',
  props: ['product', 'view-type'],
  components: {
    productActionBar
  },
  data() {
    return {}
  },
  created() {},
  watch: {},
  computed: {},
  methods: {
    removeProduct() {
      this.$emit('removeProduct');
    }
  }
}
