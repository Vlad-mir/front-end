export default {
  name: 'product-action-add-to-cart',
  props: [
    'sku-id'
  ],
  data() {
    return {}
  },
  created() {},
  watch: {},
  computed: {},
  methods: {
    addToCart() {
      this.$root.$emit('add-product-to-cart', {skuId: this.$props.skuId});
    }
  }
}
