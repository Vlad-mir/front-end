export default {
  name: 'breadcrumbs',
  props: [],
  data() {
    return {
      breadcrumbs: []
    }
  },
  created() {
    this.$nextTick(() => {
      this.getBreadCrumbs();
    });
  },
  watch: {},
  computed: {},
  methods: {
    getBreadCrumbs: function() {
      let result = [{route: 'Home', alias: 'Главная'}];
      let routes = this.$route.matched;
      if (routes && routes.length) {
        routes.map(route => {
          result.push({
            route:  route.name,
            alias:  (route.meta.alias) ? route.meta.alias : '',
            query:  (route.query)      ? route.query      : {},
            params: (route.params)     ? route.params     : {}
          });
        });
        this.breadcrumbs = Object.assign([], result);
      }
    },
    setAlias(value) {
      this.$route.meta.alias = value;
      this.getBreadCrumbs();
    }
  }
}
