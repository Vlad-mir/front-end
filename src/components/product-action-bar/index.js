import productActionAddToCart from './../product-action-add-to-cart/index.vue';
export default {
  name: 'product-action-bar',
  props: [
    'product-id',
    'sku-id',
    'category-id',
    'owner'
  ],
  components: {
    productActionAddToCart
  },
  data() {
    return {
      removeMessage: 'Вы действительно хотите удалить товар? '
      + 'Так же будут удалены все торговые предложения и остатки этого товара.'
    }
  },
  created() {},
  watch: {},
  computed: {},
  methods: {
    remove(id) {
      if (!confirm(this.removeMessage)) return false;
      this.$store.dispatch('products/removeProduct', id).then(() => {
        this.$emit('remove');
      });
    }
  }
}
