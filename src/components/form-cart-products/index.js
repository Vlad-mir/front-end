import ActionCartAmount from './../action-cart-amount/index.vue';

export default {
  name: 'form-cart-products',
  components: {
    ActionCartAmount
  },
  data() {
    return {
      loading: true,
    }
  },
  created() {
    this.getCart();
  },
  watch: {},
  computed: {
    cart() {
      return this.$store.state.cart.cart;
    }
  },
  methods: {
    getCart() {
      this.$store.dispatch('cart/getCart').then(() => this.loading = false);
    },
    removeSKUFromCart(skuID) {
      this.loading = true;

      this.$store.dispatch('cart/removeSKUFromCart', {
        skuId: skuID,
        cartCode: this.cart.code
      }).then(() => this.loading = false);

    }
  }
}
