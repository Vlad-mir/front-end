import breadcrumbs from './../../components/breadcrumbs/index.vue';
import multiAutocomplete from './../../components/multi-autocomplete/index.vue';
import editor from './../../components/editor/index.vue';
import inputFile from './../../components/input-file/index.vue';

export default {
  name: 'form-update-product',
  components: {
    breadcrumbs,
    multiAutocomplete,
    editor,
    inputFile
  },
  data () {
    return {
      removeProductMessage: 'Вы действительно хотите удалить товар? '
      + 'Так же будут удалены все торговые предложения и остатки этого товара.',
      product: {
        id: 0,
        name: '',
        code: '',
        sort: 100,
        public: 1,
        company_id: 0,
        categories: [],
        category: [],
        discounts: []
      },
      statuses: {
        1: 'Отправлен на модерацию',
        3: 'Скрыт'
      }
    }
  },
  computed: {
    id() {
      return this.$route.params.product_id;
    },
    user() {
      return this.$store.state.user.currentUser;
    },
    companies() {
      let companies = this.$store.state.user.currentUserCompanies;
      if (companies && companies.length && !this.product.company_id) {
        this.product.company_id = companies[0]['id'];
      }
      return companies;
    },
    companiesIds() {
      return (this.companies && this.companies.length) ? this.companies.map(item => item.id) : [];
    },
    categories() {
      let categories = this.$store.state.category.categories;
      if(categories && categories.length) {
        categories = categories.map(item => {
          return {text: item.name, value: item.id};
        });
      } else {
        categories = []
      }
      return categories;
    },
    discounts() {
      let discounts = this.$store.state.discount.discounts.data;
      if (discounts && discounts.length) {
        discounts = discounts.map(item => {
          return {
            text: 'Скидка ' + item.value + ' ' + ( (parseInt(item.type) === 1) ? '%' : 'р'),
            value: item.id
          };
        });
      } else {
        discounts = [];
      }
      return discounts;
    }
  },
  watch: {},
  methods: {
    init() {
      if (!this.user) this.$router.push({name: 'Login'});
      this.$nextTick(() => {
        this.getProduct();
        this.getCategories();
        this.getCompanies();
      });
    },
    getCategories(update) {
      if (!this.categories || !this.categories.length || update) {
        this.$store.dispatch('category/getCategories');
      }
    },
    getCompanies(update) {
      if (!this.companies || !this.companies.length || update) {
        this.$store.dispatch('user/getCurrentUserCompanies', this.user.id).then(() => {
          this.getDiscounts();
        });
      } else {
        this.getDiscounts();
      }
    },
    getDiscounts(update) {
      if (!this.companies || !this.companies.length) return false;
      if (!this.discounts || !this.discounts.length || update) {
        let filter = {model: [], relation: []};
        filter.model.push(['company_id', '=', this.companiesIds]);
        this.$store.dispatch('discount/getDiscounts', {filter: filter});
      }
    },
    getProduct() {
      this.setBreadCrumbs('Загрузка...');
      if (this.id !== 'new') {
        this.$store.dispatch('products/getProductByid', this.id).then(product => {
          //форматирем категории
          if (product.category && product.category.length) {
            product.categories = Object.assign([], product.category.map(item => item.id));
          }
          //форматируем скидки
          if (product.discounts && product.discounts.length) {
            product.discounts = Object.assign([], product.discounts.map(item => item.id));
          }

          product.sku = product.sku.map(item => {
            item.show = true;
            item.new = false;
            return item;
          });

          if (!product.sort) product.sort = 100;
          if (product.sku && product.sku.length) this.sku = Object.assign([], product.sku);

          this.product = Object.assign({}, product);
          if (this.product && this.product.name) {
            this.setBreadCrumbs(this.product.name);
          }

          this.$emit('input', this.product);
        });
      } else {
        this.setBreadCrumbs('Новый товар');
      }

    },

    setBreadCrumbs(value) {
      if (this.$parent.$refs.breadcrumbs) {
        this.$parent.$refs.breadcrumbs.setAlias(value);
      }
    },
    saveProduct() {
      let url = (this.id ==='new') ? 'products/postProduct' : 'products/putProduct';
      this.$store.dispatch(url, this.product).then((response) => {
        if (this.id ==='new') {
          this.$router.push({
            name: 'ProductEdit',
            params: {
              category_id: this.product.categories[0],
              product_id: response.id
            }
          });
        } else {
          this.init();
        }
        this.$emit('input', this.product);
      });
    },
    removeProduct() {
      if (!confirm(this.removeProductMessage)) return;
      this.$store.dispatch('products/removeProduct', this.product.id).then((response) => {
        this.$router.push({name: 'UserProducts'});
      }, (response) => {

      });
    }
  },
  created:  function() {
    this.init();
  }
}
