export default {
  name: 'alert-small-cart',
  props: [],
  data() {
    return {
      show: false,
      emited: false,
      CartCode: null,
      ErrorText: 'Произошла ошибка, пожалуйста, обратитесь к администратору.',
      AddTitle: 'Товар добавлен'
    }
  },
  created() {
    this.init();
  },
  watch: {},
  computed: {
    cart() {
      return this.$store.state.cart.cart;
    },
    user() {
      return this.$store.state.user.currentUser;
    }
  },
  methods: {
    init() {
      this.updateCardCode();
      this.getCart();
      this.listener();
    },
    listener() {
      this.$root.$on('add-product-to-cart', (data) => {
        //обновляем код корзины
        this.updateCardCode();
        //не даём пользователю по 10 раз кликать на кнопку покупки (блокируем её на время запроса)
        if (this.emited) return false;
        this.emited = true;

        //если не пришло SKU-ID, покажем пошибку
        if (!data.skuId) {
          this.commit('showSnackbar', [this.ErrorText, 6000]);
          return false;
        }

        //смотрим сколько уже этого СКУ есть в корзине
        let amount = this.getSKUinCartAmount(data.skuId);
        //если больше 0, то прибавляем 1 к количеству СКУ в корзине
        if (amount) amount++;

        if (data.amount) amount = data.amount;

        //добавляем/обновляем СКУ в корзине
        this.addProductToCart(data.skuId, amount, data.noPopup);
      });
    },
    getCart() {
      this.$store.dispatch('cart/getCart');
    },
    getSKUinCartAmount(skuId) {
      let amount = undefined;
      if (!this.cart || !this.cart.products || !this.cart.products.length) return amount;

      this.cart.products.map(product => {
        if (parseInt(skuId) === parseInt(product.sku_id)) {
          amount = product.amount;
        }
      });
      return amount;
    },
    addProductToCart(skuId, amount, noPopup) {
      this.$store.dispatch('cart/pushSKUToCart', {
        'sku_id': skuId,
        'cart_code': this.CartCode,
        'amount': (amount) ? amount : 1,
        'user_id': (this.user) ? this.user.id : null
      }).then(response => {
        //разблокируем кнопку покупки
        this.emited = false;
        if (response && !noPopup) this.show = true;

        this.$root.$emit('add-product-to-cart-complete', response);

      });
    },
    updateCardCode() {
      this.CartCode = localStorage.getItem('cart_code');
    }
  }
}
