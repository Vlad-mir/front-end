export default {
  name: 'form-signup',
  props: [],
  data() {
    return {
      //объект формы
      form: {
        email: '',
        password: '',
        password_confirm: ''
      },
      //правила валидации формы
      rules: {
        email: {
          required: true,
          email: true
        },
        password: {
          required: true
        },
        password_confirm: {
          required: true
        },
        check: {
          required: true
        }
      }
    }
  },
  created() {
    if (this.currentUser && this.currentUser.id) {
      this.$router.push({name: 'UserEvents'})
    }
  },
  watch: {},
  computed: {
    /**
     * Вернёт текущего пользователя из стора
     * @returns {computed.currentUser|boolean}
     */
    currentUser() {
      return this.$store.state.user.currentUser;
    },
  },
  methods: {
    /**
     * Валидирует форму и авторизует пользователя
     * @param e - событие submit'a формы
     */
    submit(e) {
      e.preventDefault();
      this.$validator.validateAll().then((result) => {
        if (result) {
          this.$store.dispatch('user/signup', this.form).then(response => {
            if (response) document.location.reload(true);
          });
        }
      });
    }
  }
}
