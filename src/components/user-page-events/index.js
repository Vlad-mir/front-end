import eventProductItemHorizontal from './../../components/event-product-item-horizontal/index.vue';
import eventNewsItemHorizontal    from './../../components/event-news-item-horizontal/index.vue';
import eventActionItemHorizontal  from './../../components/event-action-item-horizontal/index.vue';
import paginate                   from 'vuejs-paginate';

export default {
  name: 'user-page-events',
  props: [],
  components: {
    eventProductItemHorizontal,
    eventNewsItemHorizontal,
    eventActionItemHorizontal,
    paginate
  },
  data() {
    return {
      per_page: 10,    //элементов на странице
      current_page: 0 //текущая страница (чтобы установить первую страницу компоненту надо передать 0 ¯\_(ツ)_/¯)
    }
  },
  created() {
    this.getEvents();
  },
  watch: {},
  computed: {
    /**
     * Все события
     * @returns {Array}
     */
    events() {
      return this.$store.state.log.userEvents.data;
    },
    /**
     * Всего событий
     * @returns {number}
     */
    total() {
      return this.$store.state.log.userEvents.total;
    },
    /**
     * Последняя страница
     * @returns {number}
     */
    lastPage() {
      return this.$store.state.log.userEvents.last_page;
    }
  },
  methods: {
    /**
     * Получение страничного вывода списка событий
     * @param count - количество событий на странице
     * @param page  - страница, которую надо показать
     */
    getEvents(count, page) {
      count = (count) ? count : this.per_page;
      page  = (page)  ? page  : this.current_page;
      this.$store.dispatch('log/getUserEvents', {count: count, page: page});
    },
    /**
     * Событие, вызывающееся когда юзер кликает по пагинатору
     * @param page - номер страницы, на который нажал юзер
     */
    paginatorAction(page) {
      this.getEvents(this.per_page, page);
    }
  }
}
