import css from './index.css';

export default {
  name: 'input-file',
  props: [
    'value',
    'entity',
    'required',
    'multiple'
  ],
  components: {},
  data () {
    return {
      file: {},
      preview: []
    }
  },
  computed: {},
  watch: {
    'value'() {
      this.getPreview();
    }
  },
  methods: {
    change(e) {
      if (!e.target.files || ! e.target.files.length) {
        return false;
      }

      this.file = Object.assign({}, e.target.files);

      if (this.multiple) {
        this.$emit('input', this.file);
      } else {
        this.$emit('input', this.file[0]);
      }
    },
    getPreview() {
      this.preview = [];
      this.files = [];
      let files = [];
      if (this.multiple) {
        files = Object.assign([], this.value);
      } else {
        files.push(this.value);
      }

      files.map(file => {
        //если есть id, значит в value объект,
        //который пришёл с БЕ, а не объект нового файла
        if (file && file.id) {
          this.preview.push(file);
        }
      })
    },
    removeFile(file) {
      if (!confirm('Вы действительно хотите удалить изображение?')) return false;
      this.$emit('remove', {image: file, entity: this.entity});
      this.$emit('input', {});
    }
  },
  created: function() {
    this.$nextTick(() => {
      if (this.value) this.getPreview();
    });
  },

}
