import ckeditor from '@ckeditor/ckeditor5-build-classic';

export default {
  name: 'editor',
  props:['value', 'id'],
  components: {},
  data () {
    return {
      editor: null,
      editValue: '',
      oldValue: '',
      settings: {
        toolbar: [
          'bold',
          'italic', '|',
          'blockQuote',
          'link', '|',
          'bulletedList',
          'numberedList', '|'
        ]
      }
    }
  },
  computed: {

  },
  watch: {},
  methods: {
    getData() {
      if (this.editor) {
        return this.editor.getData();
      }
    },
    getChanges() {
      if (this.editor) {
        let newValue = this.editor.getData();
        if (this.oldValue !== newValue) {
          this.oldValue = newValue;
          return this.oldValue;
        }
        return false;
      }
    },
    listen() {
      //Мега-костыль потому что ckeditor не вызывает своё же событие change
      //это по факту не используется, но пригодится может
      // внешние компоненты используют функцию getChanges()
      setInterval(() => {
        let newVal = this.getChanges();
        if (newVal) this.$emit('input', newVal);
      }, 1000);
    },
    startEditor() {
      let area = document.querySelector('#editor-area-' + this.id);
      this.editValue = this.value;
      ckeditor.create(area, this.settings).then(editor => {
          this.editor = editor;
          if (this.editValue) this.editor.setData(this.value);
          this.listen();
        }).catch(error => {
          this.$store.commit('showSnackbar',
            ['Произошла ошибка, пожалуйста, попробуйте позже.', 60000]
          );
        });
    }
  },
  created:  function() {
    this.$nextTick(() => this.startEditor());
  },

}
