import productListSettings   from './../product-list-settings/index.vue';
import productItem from './../product-item/index.vue';
import paginate from 'vuejs-paginate';

export default {
  name: 'user-page-products',
  components: {
    productListSettings,
    productItem,
    paginate
  },
  data() {
    return {
      page: 1,
      per_page: 1,   //элементов на странице
      current_page: 0 //текущая страница (чтобы установить первую страницу компоненту надо передать 0 ¯\_(ツ)_/¯)
    }
  },
  created() {
    this.getCurrentUserCompanies();
  },
  watch: {},
  computed: {
    /**
     * Всего товаров
     * @returns {number}
     */
    total() {
      return this.$store.state.products.userProduct.total;
    },
    /**
     * Последняя страница
     * @returns {number}
     */
    lastPage() {
      return this.$store.state.products.userProduct.last_page;
    },
    products() {
      return this.$store.state.products.userProduct.data;
    },
    currentUser() {
      return this.$store.state.user.currentUser;
    },
    companies() {
      return this.$store.state.user.currentUserCompanies;
    },
    /**
     * Выбранный тип отображения товаров
     * @returns {computed.sortType|string|*}
     */
    sortType() {
      return this.$store.state.products.sortType;
    },
    /**
     * Выбранное направление сортировки
     * @returns {computed.viewType|string|*}
     */
    viewType() {
      return this.$store.state.products.viewType;
    },
  },
  methods: {
    /**
     * Получение списка компаний текущего пользователя
     */
    getCurrentUserCompanies() {
      if (!this.currentUser || !this.currentUser.id) {
        this.$router.push({name: 'Home'});
      }
      this.$store.dispatch('user/getCurrentUserCompanies', this.currentUser.id).then(() => {
        this.$nextTick(() => this.getProducts(this.per_page, 1));
      });
    },
    /**
     * Получение товаров из компаний, которые принадлежат текущему пользователю
     */
    getProducts(count, page) {
      if (!this.currentUser) this.$router.push({name: 'Login'});
      count = (count) ? count : this.per_page;
      page  = (page)  ? page  : this.page;
      this.$store.dispatch('products/getUserProduct', {count: count, page: page, id: this.currentUser.id});
    },
    /**
     * Событие, вызывающееся когда юзер кликает по пагинатору
     * @param page - номер страницы, на который нажал юзер
     */
    paginatorAction(page) {
      this.page = page;
      this.getProducts(this.per_page, page);
    }
  }
}
