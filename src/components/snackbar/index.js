export default {
  name: 'snackbar',
  props: [],
  components: {},
  data() {
    return {}
  },
  created() {},
  watch: {},
  computed: {
    snackbar() {
      return this.$store.state.snackbar;
    },
  },
  methods: {
    hideSnackBar() {
      this.$root.$store.commit('hideSnackbar');
    }
  }
}
