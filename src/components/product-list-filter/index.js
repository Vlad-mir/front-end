export default {
  name: 'product-list-filter',
  components: {},
  props: [],
  data() {
    return {
      propertyToggler: {},
      filterValue: {},
      showFilter: false,
    }
  },
  created() {
    this.getProperties(this.currentCategoryId);

    //получим категории, если их нет
    if (!this.categories || !this.categories.length) {
      this.$store.dispatch('category/getCategories');
    }

    //если есть фильтр, то парсим его, если нет, то сбрасываем
    if (this.$route.query && this.$route.query.filter) {
      this.filterValue = JSON.parse(this.$route.query.filter);
    } else {
      this.filterValue = {};
    }
    this.changeFilter();
  },
  watch: {
    /**
     * Обновляем список свойств
     * когда меняется выбранная категория
     * @param id
     */
    '$route.params.category_id'(id) {
      this.$emit('change_category', {'category_id': id});
      this.getProperties(id);
      this.$emit('filter', this.filterValue);
      this.changeFilter();
    }
  },
  computed: {
    /**
     * ID выбранной пользователем категории,
     * если не выбрана то нужно вернуть именно null, это важно
     * P.S. Смотри функцию viewCategories()
     * @returns {null}
     */
    currentCategoryId() {
      let category = this.$route.params.category_id;
      if (!category || category === 'all') {
        category = null;
      }
      return category;
    },

    /**
     * Выбранная пользователем категория
     * @returns {{}}
     */
    currentCategory() {
      let cat = {};
      if (this.categories && this.categories.length) {
        this.categories.map(item => {
          if (parseInt(item.id) === parseInt(this.currentCategoryId)) {cat = item;}
        });
      }
      return cat;
    },

    /**
     * Плоский список всех категорий товаров
     * @returns {computed.categories|Array}
     */
    categories() {
      return this.$store.state.category.categories;
    },

    /**
     * Категории, которые должны отображаться в филтре
     * в данный момент, с учётом родительской категории
     */
    viewCategories() {
      return this.categories.filter(item => (
        (parseInt(item.parent_category_id) === parseInt(this.currentCategoryId)) ||
        (item.parent_category_id === null && this.currentCategoryId === null)
      ));
    },

    /**
     * Свойства товаров, доступные для текущей категории
     * @returns {Array}
     */
    properties() {
      let property = this.$store.state.category.categoryProperties;

      //создаём тугглер для открытия/скрытия фильтров и строку фильтра
      if (property && property.length) {
         property.map(item => {
           this.propertyToggler[item.id] = true;
           //если фильт уже установлен, то не затираем его
           if (!this.$route.query || !this.$route.query.filter) {
             this.filterValue[item.code] = [];
           }
         });
      } else {
        this.propertyToggler = {};
      }
      this.changeFilter();
      return property;
    }
  },
  methods: {
    /**
     * Возвращает из БД свойства товаров,
     * доступные на текущей категории
     * @param id
     */
    getProperties(id) {
      let catArray = [];
      catArray.push(id);
      this.$store.dispatch('category/getCategoryProperties', catArray);
    },

    /**
     * Строит объект фильтра исходя из того,
     * что выбрал пользователь в фильтре на странице
     * @returns {{relation: Array}}
     */
    getFilter() {
      let filter = this.$route.query.filter;
      let filterString = {relation:[]};

      if (this.currentCategoryId) {
        filterString.relation.push(
          ['categories','category_id','=', this.currentCategoryId]
        );
      }

      //если фильтр пустой
      if (!filter) {
        if (filterString.relation.length) {
          return filterString;
        } else { return; }
      }

      filter = JSON.parse(filter);
      Object.keys(filter).forEach((code, i) => {
        let item = filter[code];
        if (item && item.length) { //фильтр должен быть не пуст
          //добавляем AND, если условие не первое
          if (i) filterString.relation.push('AND');
          filterString.relation.push(['propertyValues', 'value', '=', item]);
        }
      });
      return filterString;
    },

    /**
     * Обновляет и инициализует фильтр
     */
    filter() {
      //если фильтр пуст
      if (!this.filterValue && this.filterValue.length) return;
      let filter = {};
      //отбираем только не пустые условия
      Object.keys(this.filterValue).map(i => {
        if (this.filterValue[i] && this.filterValue[i].length) {
          filter[i] = this.filterValue[i];
        }
      });
      //если нет непустых условий, то выходим
      if (!Object.keys(filter).length) return;

      //обновим страницу уже с фильтром
      this.$router.push({
        name: this.$route.name,
        query: {
          filter: JSON.stringify(filter)
        }
      });
      this.$emit('filter', this.filterValue);
      this.changeFilter();
    },

    /**
     * Сбрасывает фильтр
     */
    clearFilter() {
      this.$router.push({name: this.$route.name});
      this.$emit('filter', this.filterValue);
      this.changeFilter();
    },

    /**
     * Обновляет флаг показа/скрытия фильтра
     */
    changeFilter() {
      let filter = this.filterValue;
      this.showFilter = false;
      Object.keys(filter).map(i => {
        if (filter[i].length) this.showFilter = true;
      });
    },

  }
}
