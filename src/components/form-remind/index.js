export default {
  name: 'form-login',
  props: [],
  data() {
    return {
      //объект формы
      from: {
        email: '',
      },
      //правила валидации формы
      rules: {
        email: {
          required: true,
          email: true
        }
      }
    }
  },
  created() {
    if (this.currentUser && this.currentUser.id) {
      this.$router.push({name: 'UserEvents'})
    }
  },
  watch: {},
  computed: {
    /**
     * Вернёт текущего пользователя из стора
     * @returns {computed.currentUser|boolean}
     */
    currentUser() {
      return this.$store.state.user.currentUser;
    },
  },
  methods: {
    /**
     * Валидирует форму и авторизует пользователя
     * @param e - событие submit'a формы
     */
    submit(e) {
      e.preventDefault();
      this.$validator.validateAll().then((result) => {
        if (result) {
          this.$store.dispatch('user/remind', this.from.email).then(response => {
            //if (response) this.$router.push({name: 'login'});
            console.log(response);
          });
        }
      });
    }
  }
}
