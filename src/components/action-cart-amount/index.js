export default {
  name: 'action-cart-amount',
  props: [
    'amount', 'skuId'
  ],
  components: {},
  data() {
    return {
      amountValue: 0,
      loading: false
    }
  },
  created() {
    if (this.$props.amount) this.amountValue = this.$props.amount;
  },
  watch: {
    '$props.amount'(val) {
      if (val) this.amountValue = val;
    },
  },
  computed: {},
  methods: {
    changeSKUAmount(amount) {
      //если идёт загрузка, значит ничего не делаем
      if (this.loading) return false;

      if (amount < 1)  amount = 1;
      if (amount > 99) amount = 99;
      this.amountValue = amount;

      //ставим флаг загрузки
      this.loading = true;

      this.$root.$emit('add-product-to-cart', {
        skuId:   this.$props.skuId,
        amount:  this.amountValue,
        noPopup: true
      });

      //снимаем флаг загрузки когда запрс отработал
      this.$root.$on('add-product-to-cart-complete', () => {
        this.loading = false;
      });
    }
  }
}
