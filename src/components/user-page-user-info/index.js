export default {
  name: 'user-page-user-info',
  props: [],
  components: {},
  data() {
    return {}
  },
  created() {},
  watch: {},
  computed: {
    user() {
      return this.$store.state.user.currentUser;
    }
  },
  methods: {

  }
}
