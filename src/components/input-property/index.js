import inputSelect from './../input-select/index.vue';
import inputFileMultiple from './../input-file-multiple/index.vue';

export default {
  name: 'input-property',
  props: [
    'sku-id',
    'categories'
  ],
  components: {
    inputSelect,
    inputFileMultiple
  },
  data () {
    return {
      values: {}
    }
  },
  computed: {
    properties() {
      return this.$store.state.category.categoryProperties;
    },
    propValues() {
      return this.$store.state.category.propertiesValues;
    }
  },
  watch: {
    'categories'() {
      this.getProperties();
    },
    'skuId'() {
      this.getValues();
    }
  },
  methods: {
    save(data) {
      let formData = new FormData();
      let newValues = {};
      this.properties.map((property, i) => {
        newValues[property.code] = property;
        newValues[property.code]['property_id'] = property.id;
        newValues[property.code]['entity_id'] = data.id;
        newValues[property.code]['value'] = this.values[property.id];

        if (property.field_type === 'file') {
          if (property.multiply) {
            Object.keys(this.values[property.id]).map(key => {
              formData.append(property.code + '[]', this.values[property.id][key]);
            })
          } else {
            formData.append(property.code + '[]', this.values[property.id]);
          }
        } else {
          formData.append(property.code, this.values[property.id]);
        }
      });
      formData.append('_request_meta_data', JSON.stringify(newValues));
      this.$store.dispatch('properties/postPropertiesValue', formData).then(() => this.getValues());
    },
    removeFile(data) {
      this.$store.dispatch('properties/deleteImage', {
        property_id: data.entity.id,
        entity_id:   this.$props.skuId,
        value:       data.image.id
      }).then(() => this.getValues());
    },
    change() {
      this.$emit('change', this.values);
      this.$emit('input', this.values);
    },
    changeValues() {
      if (this.$props.skuId) {
        this.values = Object.assign({}, this.propValues[this.$props.skuId]);
        this.change();
      }
    },
    getProperties() {
      this.$store.dispatch('category/getCategoryProperties', this.$props['categories']);
    },
    getValues() {
      if (!this.$props.skuId) return false;
      this.$store.dispatch('category/getSKUPropertiesValues', this.$props.skuId).then(() => {
        this.changeValues();
      });
    }
  },
  created:  function() {
    this.$nextTick(() => {
      this.getProperties();
      this.getValues();

      this.$on('save', (data) => {
        this.save(data);
      });

    });
  },
}
