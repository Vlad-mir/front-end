import formSmallLogin from './../form-small-login/index.vue';

export default {
  name: 'small-user-dropdown',
  props: [],
  components: {formSmallLogin},
  data() {
    return {
      dropdownShow: false,
    }
  },
  created() {},
  watch: {},
  computed: {
    currentUser() {
      return this.$store.state.user.currentUser;
    },
  },
  methods: {
    logout(){
      localStorage.removeItem('id_token');
      localStorage.removeItem('current_user');
      localStorage.removeItem('cart_code');
      document.location.reload();
    }
  }
}
