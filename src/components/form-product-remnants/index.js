import editor from './../../components/editor/index.vue';
import inputFile from './../../components/input-file/index.vue';

export default {
  name: 'form-product-remnants',
  props: [
    'value',
    'product-id',
    'company-id'
  ],
  components: {
    editor,
    inputFile,
  },
  data () {
    return {
      removeMessage: 'Вы действительно хотите удалить остаток?',
      resultValue: [],
    }
  },
  computed: {
    sku() {
      let product = this.$store.state.products.product, sku = [];
      if (product.sku && product.sku.length) sku = Object.assign([], product.sku);
      return sku;
    },
    stocks() {
      return this.$store.state.stock.stocks;
    },
    user () {
      return this.$store.state.user.currentUser;
    }
  },
  watch: {
    'productId'() {
      this.getRemnants();
    },
    'companyId'() {
      this.getStocks();
    }
  },
  methods: {
    change() {
      this.getRemnants();
      this.$emit('input', this.resultValue);
      this.$emit('change', this.resultValue);
    },
    getSKU() {
      if (!this.sku || !this.sku.length) {
        this.$store.dispatch('products/getProductByid', this.$props.productId);
      }
    },
    getRemnants() {
      this.$store.dispatch('stock/getProductRemnants', this.$props.productId).then(response => {
        let remnants = response.map(item => {
          item.show = true;
          item.new  = false;
          return item;
        });
        if (remnants && remnants.length) this.resultValue = Object.assign([], remnants);
        this.$emit('change', this.resultValue);
      });
    },
    getStocks() {
      this.$store.dispatch(
        'stock/getUserStock', {
          company_id: this.$props.companyId,
          user_id: this.user.id
        }
      );
    },
    addRemnants() {
      if (!this.resultValue) {
        this.resultValue = [];
      }
      this.resultValue.push({
        id: null,
        sku_id: 0,
        stock_id: 0,
        amount: 1,
        show: true,
        new: true
      });
    },
    removeRemnant(item, i) {
      if (!confirm(this.removeMessage)) return false;
      if (item.id) {
        this.$store.dispatch('stock/removeRemnant', item.id).then(() => this.getRemnants());
      } else {
        this.resultValue.splice(i,1);
      }
    },
    saveRemnant(item, i) {
      let url = (item.id) ? 'stock/putRemnant' : 'stock/postRemnant';
      this.$store.dispatch(url, item).then(() => this.getRemnants());
    },
    toggle(i) {
      if (this.resultValue && this.resultValue.length) {
        this.resultValue = this.resultValue.map((item, k) => {
          if (k === i) item.show = !item.show;
          return item;
        });
      }
    }
  },
  created:  function() {
    this.$nextTick(() => {
      this.getRemnants();
      this.getSKU();
      this.getStocks();
    });
  },
}
