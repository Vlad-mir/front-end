import smallSearch         from './../small-search/index.vue';
import smallCart           from './../small-cart/index.vue';
import smallUserDropdown   from './../small-user-dropdown/index.vue';
import headerCategoryMenu  from './../header-category-menu/index.vue';

export default {
  name: 'b-header',
  props: [],
  components: {
    smallSearch,
    smallCart,
    smallUserDropdown,
    headerCategoryMenu
  },
  data() {
    return {}
  },
  created() {},
  watch: {},
  computed: {
    location() {
      return this.$store.state.location;
    },
    moduleSettings() {
      return this.$store.state.moduleSettings;
    },
  },
  methods: {}
}
