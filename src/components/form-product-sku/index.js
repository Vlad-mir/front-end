import editor from './../../components/editor/index.vue';
import inputFile from './../../components/input-file/index.vue';
import inputProperty from './../../components/input-property/index.vue';

export default {
  name: 'form-product-sku',
  props: [
    'value',
    'product-id',
    'categories'
  ],
  components: {
    editor,
    inputFile,
    inputProperty
  },
  data () {
    return {
      removeSKUMessage: 'Вы действительно хотите удалить торговое предложение?',
      skuStatuses: {
        2: 'Отправлен на модерацию',
        1: 'Скрыто'
      },
      resultValue: [],
      propertiesValues: {}
    }
  },
  computed: {
    user() {
      return this.$store.state.user.currentUser;
    },
  },
  watch: {
    'productId'() {
      this.getProduct();
    }
  },
  methods: {
    change() {
      this.getProduct();
      this.$emit('input', this.resultValue);
      this.$emit('change', this.resultValue);
    },
    getProduct() {
      this.$store.dispatch('products/getProductByid', this.$props.productId).then(product => {
        let sku = product.sku.map(item => {
          item.show = true;
          item.new  = false;
          this.propertiesValues[item.id] = {};
          return item;
        });
        if (sku && sku.length) this.resultValue = Object.assign([], sku);
        this.$emit('change', this.resultValue);
      });
    },

    removeSkuHeroImage(data) {
      this.$store.dispatch('sku/deleteSKUHeroImage', {
        sku_id: data.entity.id,
        img_id: data.image.id
      });
    },
    addSku() {
      if (!this.resultValue) {
        this.resultValue = [];
      }
      this.resultValue.push({
        id: null,
        name: '',
        description: '',
        code: '',
        sort: 100,
        status: 2,
        product_id: this.$props.productId,
        price: 0.00,
        default: (this.resultValue.length) ? 0 : 1,
        new: true,
        show: true,
        hero_image: {}
      });
    },
    saveSku(sku, i) {
      if (sku.hero_image && sku.hero_image.id) delete sku.hero_image;
      let data = new FormData();
      Object.keys(sku).map(key => data.append(key, sku[key]));

      this.$store.dispatch((parseInt(sku.id)) ? 'sku/putSKU' : 'sku/postSKU', data).then((response) => {
        this.change();
        this.$refs['properties-' + i][0].$emit('save', response);
      });
    },
    removeSkU(sku) {
      if (!sku || !sku.id) return;
      if (!confirm(this.removeSKUMessage)) return;
      this.$store.dispatch('sku/deleteSKU', sku.id).then(() => {
        this.change();
      });
    },
    toggleSku(i) {
      if (this.resultValue && this.resultValue.length) {
        this.resultValue = this.resultValue.map((item, k) => {
          if (k === i) item.show = !item.show;
          return item;
        });
      }
    }
  },
  created:  function() {
    this.$nextTick(() => {
      this.getProduct();
    });
  },
}
